var gulp = require('gulp');
var browsersync = require('browser-sync').create();
var sass = require('gulp-sass');
const del = require("del");
const plumber = require("gulp-plumber");

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: "./"
        },
        port: 3000
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Watch files
function watchFiles() {
    gulp.watch("./scss/**/*", css);
    gulp.watch("./**/*.html", css);

}

function css() {
    return gulp
        .src("./scss/main.scss")
        .pipe(plumber())
        .pipe(sass())
        .pipe(gulp.dest("./"))
        .pipe(browsersync.stream());
}

// Clean assets
function clean() {
    return del(["./_site/assets/"]);
}
const watch = gulp.parallel(watchFiles, browserSync);
exports.watch = watch;
exports.css = css;