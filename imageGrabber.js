window.onload = getImageDir;

function getImageDir(){
    var json = httpGet('http://192.168.178.23:8888/backend/galeryList.php');
    var object = JSON.parse(json);
    object.forEach(element => {
        buildPreviewEntry(element.name, element.images);
    });
}

function buildImageElement(path, dom){

 var img = document.createElement('img');
 img.src = path;
img.classList.add('img-item');
    dom.append(img);

}


function onImageClick(){

}

//TOTO Build Preview
function buildPreviewEntry(name, gallery){
    var dom = document.createElement("div");
    dom.classList.add("preview-entry");

    var h1 = document.createElement('h3');
    h1.innerText = name;
    dom.append(h1);


    var imgList = document.createElement('div');
    imgList.classList.add('preview-img-list');

    var items = gallery.slice(0, 1)
    items.forEach(imgPath => {
        var img = document.createElement('img');
        img.src = imgPath;
        imgList.append(img);
    });
    dom.append(imgList);

    dom.onclick = () => { buildGallery(name)};

    document.getElementById('gallery').append(dom);
}

function cleanGallery() {

    var myNode = document.getElementById('gallery');
    myNode.innerHTML = '';
}

function RebuildPreview() {
    window.removeEventListener("scroll", handleScolle);
    cleanGallery();
    getImageDir();
}

function handleScolle(mybutton) {
    let y = window.scrollY;

    // If the scroll value is greater than the window height, let's add a class to the scroll-to-top button to show it!
    if (y > 0) {
        mybutton.classList.add("show");
    } else {
        mybutton.classList.remove("show");
    }

}

function handleToTopClick(ev) {
    document.body.scrollTop = 0; // For Safari
    document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE
}

//TODO Build Clickable Gallery
function buildGallery(name){
    console.log('build '+ name);
    var json = httpGet('http://192.168.178.23:8888/backend/galeryList.php');
    var object = JSON.parse(json);

    cleanGallery();

    object.forEach(element => {
        if (element.name === name){
            var dom = document.createElement("div");
            dom.classList.add("gallery-entry");

            var backContainer = document.createElement("div")
            backContainer.classList.add("gallery-back-button");

            var icon = document.createElement("i");
            icon.classList.add("fa")
            icon.classList.add("fa-arrow-left")
            backContainer.append(icon)

            var back = document.createElement('h2');
            back.innerText = 'Zurück zur Übersicht';
            back.classList.add('gallery-back-btn');
            back.onclick = () => RebuildPreview();
            backContainer.append(back);

            dom.append(backContainer);

            var h1 = document.createElement('h1');
            h1.innerText = name;
            dom.append(h1);



            var imgList = document.createElement('div');
            imgList.classList.add('img-list');

            element.images.forEach(imgPath => {
                buildImageElement(imgPath, imgList);
            });

            var backtoTop = document.createElement("i");
            backtoTop.id = "back-to-top-button";
            backtoTop.classList.add("back-to-top")
            backtoTop.classList.add("fa")
            backtoTop.classList.add("fa-arrow-up")
            window.addEventListener("scroll", ev => {
                handleScolle(backtoTop);
            });
            backtoTop.onclick = ev => { handleToTopClick(ev)}
            dom.append(backtoTop)

            dom.append(imgList);
            document.getElementById('gallery').append(dom);
        }
    });
}

function httpGet(theUrl)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}