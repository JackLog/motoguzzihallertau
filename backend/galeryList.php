<?php 

$imageArray = array();

$tmp = array_filter(glob('../gallery/*'), 'is_dir');
usort($tmp, create_function('$a,$b', 'return filemtime($b) - filemtime($a);'));

foreach ($tmp as $gallery){
    $imagesTmp = glob("$gallery/*.{jpg,png,bmp,JPG,tif}", GLOB_BRACE);
    $imageArray[] = (object) [
        'name' => basename($gallery),
        'images' => $imagesTmp,
    ];
}

echo json_encode($imageArray);

?>